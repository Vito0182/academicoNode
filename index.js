const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const app = express();
const porta = 3000;
const bodyParser = require('body-parser');
const { exec } = require('child_process');

const Aluno = "Vitor Matheus do Nascimento Moreira";
const Turma = "AUT-2D1";

var VNode = '';
var versao = exec('node -v',(error, stdout, stderr) => {
    return `${stdout}`;
})
versao.stdout.on('data', function(data) { 
    //console.log('versao: '+data.toString()); 
    VNode = data.toString();
} ) 

let l_alunos = (async() => {
    const db = require('./db');

    //db.connect();
    console.log('inicializado!');
    const alunos = await db.selectAlunos();
    //console.log(alunos);

    return alunos;
})();

var listaAlunos;
l_alunos.then((resp) => {
    listaAlunos = resp;
})

app.use(expressLayouts);
app.set('view engine','ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.get('/',function(req,res){
    res.render('principal', {
        titulo: 'Academico2',
        Aluno: Aluno,
        Turma: Turma,
        VNode: VNode,
    })
});

app.get('/lista',(req,res) => {
  
    let arr_alunos = [
        {
            id: '1',  
            matricula: '1',   
            cpf: '1',    
            nome: 'aaaaa',
            email: '1',   
            gitlab: '1',   
            celular: '1',   
            turma: '1'  
        },
        {
            id: '2',  
            matricula: '2',   
            cpf: '2',    
            nome: 'bbbbbb',
            email: '2',   
            gitlab: '2',   
            celular: '2',   
            turma: '2'  
        }
    ];
    res.render('lista', {
        titulo: 'Academico2',
        Aluno: Aluno,
        Turma: Turma,
        VNode: VNode,
        alunos: listaAlunos
        //alunos: arr_alunos
    });

});

app.get('/edita',(req,res) => {
    res.render('desenvolvimento', {
        titulo: 'Academico2',
        Aluno: Aluno,
        Turma: Turma,
        VNode: VNode,
    });
});

app.get('/notas',(req,res) => {
    res.render('desenvolvimento', {
        titulo: 'Academico2',
        Aluno: Aluno,
        Turma: Turma,
        VNode: VNode,
    });
});

app.get('/faltas',(req,res) => {
    res.render('desenvolvimento', {
        titulo: 'Academico2',
        Aluno: Aluno,
        Turma: Turma,
        VNode: VNode,
    });
});

app.listen(porta, function(err) {
    if(err){
        console.log('Erro: '+err);
    } else {
        console.log('Servidor rodando em localhost:'+porta)
    }

})